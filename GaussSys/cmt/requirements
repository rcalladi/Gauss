package GaussSys
version v50r0

branches cmt doc

#==================================================================
# Gauss packages
#==================================================================

# Gauss application: the application is not built on Win32
use Gauss               v50r0   Sim

# LCG interfaces to generators, until available in LCGCMT
use LCG_Settings        v84r0p1 
use lhapdf              v84r0p1 LCG_GeneratorsInterfaces
use photospp            v84r0p1 LCG_GeneratorsInterfaces
use tauolapp            v84r0p1 LCG_GeneratorsInterfaces
use pythia6             v84r0p1 LCG_GeneratorsInterfaces
use pythia8             v84r0p1 LCG_GeneratorsInterfaces
use thepeg              v84r0p1 LCG_GeneratorsInterfaces
use herwigpp            v84r0p1 LCG_GeneratorsInterfaces
use rivet               v84r0p1 LCG_GeneratorsInterfaces
use alpgen              v84r0p1 LCG_GeneratorsInterfaces
use hijing              v84r0p1 LCG_GeneratorsInterfaces
use powhegbox           v84r0p1 LCG_GeneratorsInterfaces
use crmc                v84r0p1 LCG_GeneratorsInterfaces
use yoda                v84r0p1 LCG_GeneratorsInterfaces

# Generator packages: Basic configuration
use GenCuts             v3r13   Gen
use GENSER              v16r2p1 Gen
use EvtGen              v14r7   Gen
use Mint                v4r1    Gen
use EvtGenExtras        v3r12p1 Gen
use Generators          v16r0   Gen
use LbPythia8           v12r1   Gen

# Generators packages: alternative to Pythia8 for pp collisions
use LbPythia            v15r1   Gen
use LbHerwigpp          v3r1    Gen

# Generators packages: extensions to Pythia6 .or. Pythia8
use LbHard              v1r0p1  Gen
use LbAlpGen            v6r2    Gen 
use BcVegPy             v4r0p1  Gen
use GenXicc             v2r2    Gen
use LbBcVegPy           v6r1p1  Gen
use LbGenXicc           v5r1p1  Gen
use LbHidValley         v4r1    Gen
use LbOniaPairs         v2r1p2  Gen
use LbPowheg            v4r0    Gen

# Generators packages: alternative for p-A & A-A collisions
use LbHijing            v10r0p2 Gen
use LbCRMC              v1r1    Gen

# Generators packages: LHCb sampling results for Machine Induced Background
use LbMIB               v7r0p2  Gen

# Generators packages: LHCb particle gun generator
use LbPGuns             v6r1    Gen

# Generators packages: for CEP
use SuperChic           v1r0p1  Gen
use LbSuperChic         v1r0p1  Gen
use LPair               v1r0    Gen
use LbLPair             v1r0    Gen

# Generators packages: for prompt nuclei production, e.g. Deuteron
use LbBound             v1r0    Gen

# Simulation basic packages
use GiGa        	v21r1     Sim     
use GiGaCnv     	v21r1     Sim
use SimSvc              v6r0p1    Sim
use GiGaVisUI           v6r0p2    Sim
use VisSvc              v5r8      Vis 
use ClhepTools          v2r1p3    Tools
use GaussGeo            v1r0      Sim

# Simulation specialized packages
use GaussKine           v7r0      Sim
use GaussAlgs		v8r1      Sim
use GaussTools          v20r1     Sim
use GaussRICH		v15r0     Sim
use GaussTracker	v7r0p1    Sim
use GaussCalo		v10r1     Sim
use GaussPhysics	v11r1p1   Sim
use GaussCherenkov      v5r0      Sim

# Monitoring packages
use GaussMonitor	v8r2      Sim
use CaloMoniSim         v5r0p1    Calo
use VeloGauss           v3r0p1    Velo
use MuonMoniSim         v3r0p1    Muon
use BcmMoniSim          v2r0p1    Bcm
use BlsMoniSim          v2r0p1    Bls
use VPMoniSim           v1r0      VP
use HCMoniSim           v1r0      HC

# Packages for MC generator tuning
use GenTune             v2r3      Gen  

# GMDL
use LbGDML              v1r0p2    Sim

# Visualization configuration
use XmlVis              v1r23     Vis

# Test packages for new releases or installations on new platforms
use GenTests            v1r6p2    Gen
use SimChecks           v1r8      Sim

# Allow to run the QMTests  
apply_pattern QMTest

# Declare this as a container package
apply_pattern container_package

# Allow the generation of QMTest summary 
apply_pattern QMTestSummarize

####################################################################
# Override the generation of the manifest.xml from gaudi to fix the LCG 84 tcmalloc renaming issue
####################################################################
pattern -global generate_manifest_file \
    private ; \
    macro generate_manifest_file_output "$(CMTINSTALLAREA)/$(tag)/manifest.xml" ; \
        macro generate_manifest_file_deps "FORCE" ; \
        macro generate_manifest_file_command "python $(GaussSys_root)/scripts/project_manifest.py -o $(generate_manifest_file_output) $(<package>_cmtpath)/CMakeLists.txt $(LCG_config_version) $(tag)" ; \
        macro generate_manifest_file_applied "NullCommand" \
                       container-package "CallCommand" ; \
    apply_pattern $(generate_manifest_file_applied) target=generate_manifest_file ; \
        end_private


private
action TestProject "cmt br - cmt TestPackage ; cmt qmtest_summarize"
end_private

#====================================================================

